<?php
// Set the Timezone, to make sure 'date()' does not complain.
date_default_timezone_set('Europe/Brussels');

// Output is JavaScript
header( 'Content-Type: text/javascript', true );

// Get the current date in a readable format
$sDate   = date('Y-m-d H:i:s');

// Request the server uptime from the Linux OS
$sUptime = shell_exec( 'uptime' );

// Call function to determine status color
$sColor  = loadColor( $sUptime );

// START OF OUTPUT JAVASCRIPT
?>

$('#updated').html( <?php echo javascriptQuote($sDate); ?> );
$('#loadinfo').html( <?php echo javascriptQuote($sUptime); ?> );
$('#loadinfo').css( 'background-color', <?php echo javascriptQuote($sColor); ?> );

<?php
// END OF OUTPUT JAVASCRIPT

exit;

/**
 * @param $sUptime - Output from 'uptime' command
 * @return string  - HTML Color
 */
function loadColor( $sUptime )
{
    // Get number of cores
    $iCores = trim( shell_exec('nproc') );

    // If getting number of cores failed, assume 4 cores
    if ( empty($iCores) )
    {
        $iCores = 4;
    }
        
    $sResult = 'Purple';
    if ( preg_match('/averages?:\s(\d+\.\d+)\b/',$sUptime,$aMatch) )
    {
        $fLoad = $aMatch[1];
        $fLoad = floatval( $fLoad );
        if ( $fLoad > $iCores )
        {
            $sResult = 'LightPink';
        }
        elseif ( $fLoad > ($iCores/2) )
        {
            $sResult = 'Khaki';
        }
        else
        {
            $sResult = 'LightGreen';
        }
    }
    return $sResult;
}

/**
 * @param $mInput - The string you want to pass to Javascript
 * @return string - The quoted string, quotes have been added around it.
 */
function javascriptQuote($mInput)
{
    $sResult = 'null';
    if ( is_array($mInput) )
    {
        $aVals = array();
        foreach ( $mInput as $mVal )
        {
            array_push( $aVals, javascriptQuote($mVal) );
        }
        $sResult = '[' . implode(',',$aVals) . ']';
    }
    else
    {
        $sResult = '';
        $iLen = mb_strlen($mInput);
        for ($iPos = 0; $iPos < $iLen; $iPos++)
        {
            $sChar = mb_substr($mInput, $iPos, 1);
            if ( preg_match('/^[\w -\.:,]$/',$sChar) )
            {
                $sResult .= $sChar;
            }
            else
            {
                $sResult .= sprintf("\x%02X",ord($sChar));
            }
        }
        $sResult = "'".$sResult."'";
    }
    return $sResult;
}
