<?php
// Set the Timezone, to make sure 'date()' does not complain.
date_default_timezone_set('Europe/Brussels');

// Output is JavaScript
header( 'Content-Type: text/javascript', true );

$iSleepTime  = 0.1;                      // seconds
$iMaxTime    = 120;                      // seconds, 2 min.

$iStartTime  = time();                   // seconds since unix epoch
$iEndTime    = $iStartTime + $iMaxTime;  // seconds since unix epoch
$bForce      = (empty($_REQUEST['force'])) ? 0 : 1;

// This file needs to be writable by the script.
$sStatusFile = 'lastload.txt';

$fLoad       = null;
$fLastLoad   = null;
if ( file_exists($sStatusFile) )
{
    $fLastLoad = floatval( file_get_contents($sStatusFile) );
}

do
{
    // Request the server uptime from the Linux OS
    $sUptime = shell_exec( 'uptime' );
    $fLoad = uptimeToLoad( $sUptime );
    $fLoad = round( $fLoad, 1 );
    if ( $fLoad != $fLastLoad )
    {
        break; // exit this while, no sleeping anymore.
    }
    usleep( $iSleepTime * 1000000 ); // Usleep needs miCROseconds
}
while ( time() < $iEndTime and !$bForce );

file_put_contents( $sStatusFile, $fLoad );

// Get the current date in a readable format
$sDate   = date('Y-m-d H:i:s');

// Call function to determine status color
$sColor  = loadColor( $fLoad );

// START OF OUTPUT JAVASCRIPT
?>

$('#updated').html( <?php echo javascriptQuote($sDate); ?> );
$('#loadinfo').html( <?php echo javascriptQuote('Load: '.$fLoad); ?> );
$('#loadinfo').css( 'background-color', <?php echo javascriptQuote($sColor); ?> );

<?php
// END OF OUTPUT JAVASCRIPT

exit;

function uptimeToLoad( $sUptime )
{
    $fResult = null;
    if ( preg_match('/averages?:\s(\d+\.\d+)\b/',$sUptime,$aMatch) )
    {
        $fResult = $aMatch[1];
        $fResult = floatval( $fResult );
    }
    return $fResult;
}

/**
 * @param $sUptime - Output from 'uptime' command
 * @return string  - HTML Color
 */
function loadColor( $fLoad )
{
    $sResult = 'Purple';
    if ( is_float($fLoad) )
    {
        // Get number of cores
        $iCores = trim( shell_exec('nproc') );
        // If getting number of cores failed, assume 4 cores
        if ( empty($iCores) )
        {
            $iCores = 4;
        }

        if ( $fLoad > $iCores )
        {
            $sResult = 'LightPink';
        }
        elseif ( $fLoad > ($iCores/2) )
        {
            $sResult = 'Khaki';
        }
        else
        {
            $sResult = 'LightGreen';
        }
    }
    return $sResult;
}

/**
 * @param $mInput - The string you want to pass to Javascript
 * @return string - The quoted string, quotes have been added around it.
 */
function javascriptQuote($mInput)
{
    $sResult = 'null';
    if ( is_array($mInput) )
    {
        $aVals = array();
        foreach ( $mInput as $mVal )
        {
            array_push( $aVals, javascriptQuote($mVal) );
        }
        $sResult = '[' . implode(',',$aVals) . ']';
    }
    else
    {
        $sResult = '';
        $iLen = mb_strlen($mInput);
        for ($iPos = 0; $iPos < $iLen; $iPos++)
        {
            $sChar = mb_substr($mInput, $iPos, 1);
            if ( preg_match('/^[\w -\.:,]$/',$sChar) )
            {
                $sResult .= $sChar;
            }
            else
            {
                $sResult .= sprintf("\x%02X",ord($sChar));
            }
        }
        $sResult = "'".$sResult."'";
    }
    return $sResult;
}
